from gpiozero import LED, Button
from time import sleep
import threading

class circuit:
    led0 = LED(10)
    ledA = LED(9)
    ledB = LED(11)
    ledC = LED(0)
    ledD = LED(5)
    ledE = LED(6)
    ledF = LED(13)
    ledG = LED(19)
    btnArm = Button(2)
    btnZ1 = Button(3)
    btnZ2 = Button(4)
    btnZ3 = Button(17)
    btnZ4 = Button(27)

    armed = False
    alertZone = 0

    num = {
        '0':(0,0,0,0,0,0,1),
        '1':(1,0,0,1,1,1,1),
        '2':(0,0,1,0,0,1,0),
        '3':(0,0,0,0,1,1,0),
        '4':(1,0,0,1,1,0,0),
        '5':(0,1,0,0,1,0,0),
        '6':(0,1,0,0,0,0,0),
        '7':(0,0,0,1,1,1,1),
        '8':(0,0,0,0,0,0,0),
        '9':(0,0,0,0,1,0,0),
        '10':(0,0,0,1,0,0,0),
        '11':(1,1,1,1,1,1,0),
        '12':(1,1,1,0,1,1,1),
        '13':(1,1,1,1,0,0,1),
        '14':(0,1,1,1,1,1,1),
        }


    def nombre(self,i,tmp):
        value = str(i)
        print(value)
        self.ledA.value = self.num[value][0]
        self.ledB.value = self.num[value][1]
        self.ledC.value = self.num[value][2]
        self.ledD.value = self.num[value][3]
        self.ledE.value = self.num[value][4]
        self.ledF.value = self.num[value][5]
        self.ledG.value = self.num[value][6]
        sleep(tmp)

    def arm(self):
        if self.armed == False:
            for i in range(0,11):
                self.nombre(i,1)
            print('Armed')           
        else:        
            alertZone = 0
            self.nombre(1,0.5)
            self.nombre(12,0.5)
            self.nombre(13,0.5)
            self.nombre(14,0.5)
            self.nombre(11,0.5)
            self.nombre(0,0.5)
            print('unarmed')           
        self.armed = not self.armed        
        
        
        
    def actived(self,nZone):
        print('Alarm Zone: ', nZone)  
        if  self.armed == True:
            if self.alertZone == 0:
                self.alertZone = nZone
                for i in range(10,0,-1):
                    self.nombre(i,1)
                self.nombre(11,1)
                self.nombre(nZone,1)
                print('Alarm Zone: ', nZone)
            else:
                if self.alertZone == nZone:
                    self.arm()
              
    def runalarm(self):
        self.btnArm.when_pressed = lambda: self.arm()      
        self.btnZ1.when_pressed = lambda: self.actived(1)
        self.btnZ2.when_pressed = lambda: self.actived(2)
        self.btnZ3.when_pressed = lambda: self.actived(3)
        self.btnZ4.when_pressed = lambda: self.actived(4)

    #if __name__=="__main__":
    def __init__(self):
        self.btnArm.when_pressed = lambda: self.runalarm()
        
                    
circuit()
